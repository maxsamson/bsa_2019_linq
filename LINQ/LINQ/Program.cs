﻿using LINQ.MenuPanel;
using LINQ.Models;
using LINQ.ServiceSettings;
using System;
using System.Collections.Generic;

namespace LINQ
{
    class Program
    {
        public static List<Menu> ListOfMenus = new List<Menu>();
        public static Service service;

        static void Main(string[] args)
        {
            ListOfMenus.Add(new Menu(StartService, Exit));
            ListOfMenus.Add(new Menu(CreateHierarhy, TasksCount, TasksWithNameLess45Symbols, TasksFinished2019, Teams12yearsGroup, UsersACSTasksDSC, UserStruct, ProjectStruct, Exit));
            ListOfMenus.Add(new Menu(TurnBack));
            ListOfMenus[0].Show(false);
        }

        private static void Exit()
        {
            Environment.Exit(0);
        }

        private static void StartService()
        {
            Console.Clear();
            service = new Service();
            ListOfMenus[1].Show(false);
        }

        private static void TurnBack()
        {
            Console.Clear();
            ListOfMenus[1].Show(false);
        }

        private static void CreateHierarhy()
        {
            Console.Clear();
            try
            {
                var result = service.CreateHierarhy();
                foreach (var item in result)
                {
                    Console.WriteLine($"Project id: {item.Id};");
                    Console.WriteLine($"project name: \"{item.Name}\";");
                    Console.WriteLine($"author id: {item.Author_Id};");
                    Console.WriteLine($"author name: \"{item.Author.First_Name} {item.Author.Last_Name}\";");
                    Console.WriteLine($"team id: {item.Team_Id};");
                    Console.WriteLine($"team name: \"{item.Team.Name}\";");
                    Console.WriteLine("Tasks:");
                    int i = 1;
                    foreach (var task in item.Tasks)
                    {
                        Console.WriteLine($"{i++}. Task id: {task.Id}; task name: \"{task.Name}\".");
                    }
                    Console.WriteLine("----------------------------------------------------------------------");
                }
            }
            catch (Exception)
            {
                Console.WriteLine("You haven't been wrote an id, try again.");
            }
            ListOfMenus[2].Show(false);
        }

        private static void TasksCount()
        {
            Console.Clear();
            Console.WriteLine("Write user's id: ");
            try
            {
                int id = Convert.ToInt32(Console.ReadLine());
                var result = service.TasksCount(id);
                if (result.Count == 0)
                    Console.WriteLine("The user with this id has no projects.");
                else
                    foreach (var item in result)
                    {
                        Console.WriteLine($"Project id: {item.proj.Id};");
                        Console.WriteLine($"Project name: \"{item.proj.Name}\";");
                        Console.WriteLine($"Tasks count: {item.TasksCount}.");
                        Console.WriteLine("------------------------------------------");
                    }
            }
            catch (Exception)
            {
                Console.WriteLine("You haven't been wrote an id, try again.");
            }
            ListOfMenus[2].Show(false);
        }

        private static void TasksWithNameLess45Symbols()
        {
            Console.Clear();
            Console.WriteLine("Write user's id: ");
            try
            {
                int id = Convert.ToInt32(Console.ReadLine());
                var result = service.TasksWithNameLess45Symbols(id);
                if (result.Count == 0)
                    Console.WriteLine("There are no tasks that satisfy this condition.");
                else
                    foreach (var item in result)
                    {
                        Console.WriteLine($"Task id: {item.Id};");
                        Console.WriteLine($"Task name: \"{item.Name}\".");
                        Console.WriteLine("-------------------------------------");
                    }
            }
            catch
            {
                Console.WriteLine("You haven't been wrote an id, try again.");
            }
            ListOfMenus[2].Show(false);
        }

        private static void TasksFinished2019()
        {
            Console.Clear();
            Console.WriteLine("Write user's id: ");
            try
            {
                int id = Convert.ToInt32(Console.ReadLine());
                var result = service.TasksFinished2019(id);
                if (result.Count == 0)
                    Console.WriteLine("The user has no such tasks.");
                else
                    foreach(var item in result)
                    {
                        Console.WriteLine($"Task id: {item.Id};");
                        Console.WriteLine($"Task name: \"{item.Name}\".");
                        Console.WriteLine("---------------------------------------");
                    }
            }
            catch
            {
                Console.WriteLine("You haven't been wrote an id, try again.");
            }
            ListOfMenus[2].Show(false);
        }

        private static void Teams12yearsGroup()
        {
            try
            {
                Console.Clear();
                var result = service.Teams12yearsGroup();
                if (result.Count == 0)
                    Console.WriteLine("There are no teams that satisfy this condition.");
                else
                    foreach (var item in result)
                    {
                        Console.WriteLine($"Team id: {item.Id};");
                        Console.WriteLine($"Team name: \"{item.Name}\";");
                        Console.WriteLine("Users:");
                        int i = 1;
                        foreach(var user in item.Users)
                        {
                            Console.WriteLine($"{i++}. User id: {user.Id}; User name: \"{user.First_Name} {user.Last_Name}\".");
                        }
                        Console.WriteLine("----------------------------------------");
                    }
            }
            catch
            {
                Console.WriteLine("You haven't been wrote an id, try again.");
            }
            ListOfMenus[2].Show(false);
        }

        private static void UsersACSTasksDSC()
        {
            try
            {
                Console.Clear();
                var result = service.UsersACSTasksDSC();
                foreach(var item in result)
                {
                    Console.WriteLine($"User id: {item.User.Id};");
                    Console.WriteLine($"User name: \"{item.User.First_Name} {item.User.Last_Name}\";");
                    Console.WriteLine("Tasks:");
                    int i = 1;
                    foreach(var task in item.Tasks)
                    {
                        Console.WriteLine($"{i++}. Task id: {task.Id}; Task name: \"{task.Name}\".");
                    }
                    Console.WriteLine("--------------------------------------------");
                }
            }
            catch
            {
                Console.WriteLine("You haven't been wrote an id, try again.");
            }
            ListOfMenus[2].Show(false);
        }

        private static void UserStruct()
        {
            Console.Clear();
            Console.WriteLine("Write user's id: ");
            try
            {
                int id = Convert.ToInt32(Console.ReadLine());
                var result = service.UserStruct(id);
                foreach(var item in result)
                {
                    Console.WriteLine($"User id: {item.User.Id};");
                    Console.WriteLine($"User name: \"{item.User.First_Name} {item.User.Last_Name}\";");
                    Console.WriteLine($"Last project id: {item.LastProject.Project.Id};");
                    Console.WriteLine($"Last project name: \"{item.LastProject.Project.Name}\";");
                    Console.WriteLine($"Last project's tasks count: {item.LastProjectTasksCount}");
                    Console.WriteLine($"Not finished or canceled tasks:");
                    int i = 1;
                    foreach (var task in item.NotFinishedCanceledTasks)
                    {
                        Console.WriteLine($"{i++}. Task id: {task.Id}; task name: \"{task.Name}\".");
                    }
                    Console.WriteLine($"Longest task id: {item.MaxDurationTask.Id};");
                    Console.WriteLine($"Longest task name: \"{item.MaxDurationTask.Name}\".");
                    Console.WriteLine("--------------------------------------");
                }
            }
            catch
            {
                Console.WriteLine("You haven't been wrote an id, try again.");
            }
            ListOfMenus[2].Show(false);
        }

        private static void ProjectStruct()
        {
            Console.Clear();
            Console.WriteLine("Write project's id: ");
            try
            {
                int id = Convert.ToInt32(Console.ReadLine());
                var result = service.ProjectStruct(id);
                foreach (var item in result)
                {
                    Console.WriteLine($"Project id: {item.Project.Id};");
                    Console.WriteLine($"Project name: \"{item.Project.Name}\";");
                    if(item.LongestTask != null)
                    {
                        Console.WriteLine($"Longest task id: {item.LongestTask.Id};");
                        Console.WriteLine($"Longest task name: \"{item.LongestTask.Name}\";");
                    }
                    else
                        Console.WriteLine("Project has not tasks.");
                    if (item.LongestTask != null)
                    {
                        Console.WriteLine($"Shortest task id: {item.ShortestTask.Id};");
                        Console.WriteLine($"Shortest task name: \"{item.ShortestTask.Name}\";");
                    }
                    else
                        Console.WriteLine("Project has not tasks.");
                    Console.WriteLine($"Users count: \"{item.UsersCount}\".");
                    Console.WriteLine("--------------------------------------");
                }
            }
            catch
            {
                Console.WriteLine("You haven't been wrote an id, try again.");
            }
            ListOfMenus[2].Show(false);
        }
    }
}
