﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace LINQ.ServiceSettings
{
    public static class Request
    {
        private static JsonSerializerSettings settings = new JsonSerializerSettings();
        private static readonly Uri _url = new Uri("https://bsa2019.azurewebsites.net/api/");

        static Request()
        {
            settings.DateFormatString = "YYYY-MM-DDTHH:mm:ss.FFFZ";
        }

        public static List<T> GetDataList<T>(string uri)
        {
            using (HttpClient client = new HttpClient())
            {
                var result = client.GetStringAsync(_url + uri).Result;

                return JsonConvert.DeserializeObject<List<T>>(result, settings);
            }
        }
    }
}
