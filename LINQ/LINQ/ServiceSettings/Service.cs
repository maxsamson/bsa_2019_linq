﻿using LINQ.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LINQ.ServiceSettings
{
    public class Service
    {
        public List<Project> projects;
        public List<Task> tasks;
        public List<Team> teams;
        public List<User> users;

        public Service()
        {
            projects = ProjectsInit();
            tasks = TasksInit();
            teams = TeamsInit();
            users = UsersInit();
        }

        private List<Task> TasksInit()
        {
            return Request.GetDataList<Task>("Tasks");
        }

        private List<User> UsersInit()
        {
            return Request.GetDataList<User>("Users");
        }

        private List<Team> TeamsInit()
        {
            return Request.GetDataList<Team>("Teams");
        }

        private List<Project> ProjectsInit()
        {
            return Request.GetDataList<Project>("Projects");
        }

        public dynamic CreateHierarhy()
        {
            try
            {
                return projects.GroupJoin(
                    tasks.GroupJoin(
                        users,
                        t => t.Performer_Id,
                        u => u.Id,
                        (task, user) => new
                        {
                            task.Id,
                            task.Name,
                            task.Description,
                            task.Created_At,
                            task.Finished_At,
                            task.State,
                            task.Project_Id,
                            task.Performer_Id,
                            Performer = user.Single()
                        }),
                    pr => pr.Id,
                    t => t.Project_Id,
                    (proj, task) => new
                    {
                        proj.Id,
                        proj.Name,
                        proj.Description,
                        proj.Created_At,
                        proj.Deadline,
                        proj.Author_Id,
                        proj.Team_Id,
                        Tasks = task.ToList()
                    }).GroupJoin(
                    users,
                    pr => pr.Author_Id,
                    u => u.Id,
                    (proj, user) => new
                    {
                        proj.Id,
                        proj.Name,
                        proj.Description,
                        proj.Created_At,
                        proj.Deadline,
                        proj.Author_Id,
                        proj.Team_Id,
                        proj.Tasks,
                        Author = user.Single()
                    }).GroupJoin(
                    teams,
                    pr => pr.Team_Id,
                    t => t.Id,
                    (proj, team) => new
                    {
                        proj.Id,
                        proj.Name,
                        proj.Description,
                        proj.Created_At,
                        proj.Deadline,
                        proj.Author_Id,
                        proj.Team_Id,
                        proj.Tasks,
                        proj.Author,
                        Team = team.Single()
                    }).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public dynamic TasksCount(int user_Id)
        {
            try
            {
                return projects.GroupJoin(
                    tasks,
                    p => p.Id,
                    t => t.Project_Id,
                    (proj, task) => new
                    {
                        proj,
                        TasksCount = task.Count()
                    }).Where(p => p.proj.Author_Id == user_Id).Select(p => new
                    {
                        p.proj,
                        p.TasksCount
                    }).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public List<Task> TasksWithNameLess45Symbols(int user_Id)
        {
            try
            {
                return users.GroupJoin(
                tasks,
                u => u.Id,
                t => t.Performer_Id,
                (user, task) => new
                {
                    user,
                    Tasks = task.ToList()
                }).Where(item => item.user.Id == user_Id).SelectMany(item => item.Tasks.Where(t => t.Name.Length < 45)).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public dynamic TasksFinished2019(int user_Id)
        {
            try
            {
                return users.GroupJoin(
                tasks,
                u => u.Id,
                t => t.Performer_Id,
                (user, task) => new
                {
                    user,
                    Tasks = task.ToList()
                }).Where(item => item.user.Id == user_Id).SelectMany(item => item.Tasks.Where(t => t.State == State.Finished && t.Finished_At.Year == 2019)).Select(t => new { t.Id, t.Name}).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public dynamic Teams12yearsGroup()
        {
            try
            {
                return teams.GroupJoin(
                    users,
                    t => t.Id,
                    u => u.Team_Id,
                    (team, user) => new
                    {
                        team.Id,
                        team.Name,
                        Users = user.ToList()
                    }).Where(t => t.Users.Count > 0 && t.Users.All(u => DateTime.Now.Year - u.Birthday.Year > 12)).Select(item => new
                    {
                        item.Id,
                        item.Name,
                        item.Users
                    }).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public dynamic UsersACSTasksDSC()
        {
            try
            {
                return users.GroupJoin(
                    tasks,
                    u => u.Id,
                    t => t.Performer_Id,
                    (user, task) => new
                    {
                        User = user,
                        Tasks = task.ToList()
                    }).OrderBy(u => u.User.First_Name).Select(item => new
                    {
                        item.User,
                        Tasks = item.Tasks.OrderByDescending(t => t.Name.Length).ToList()
                    }).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public dynamic UserStruct(int user_Id)
        {
            try
            {
                return users.GroupJoin(
                    tasks,
                    u => u.Id,
                    t => t.Performer_Id,
                    (user, task) => new
                    {
                        User = user,
                        Tasks = task.ToList()
                    }).GroupJoin(
                    projects.GroupJoin(
                        tasks,
                        p => p.Id,
                        t => t.Project_Id,
                        (proj, task) => new
                        {
                            Project = proj,
                            Tasks = task.ToList()
                        }),
                    u => u.User.Id,
                    p => p.Project.Author_Id,
                    (user, proj) => new
                    {
                        user.User,
                        user.Tasks,
                        Projects = proj.ToList()
                    }).Where(item => item.User.Id == user_Id).Select(item => new
                    {
                        item.User,
                        LastProject = item.Projects.OrderByDescending(p => p.Project.Created_At).First(),
                        LastProjectTasksCount = item.Projects.OrderByDescending(p => p.Project.Created_At).First().Tasks.Count(),
                        NotFinishedCanceledTasks = item.Tasks.Where(t => t.State != State.Finished).ToList(),
                        MaxDurationTask = item.Tasks.OrderBy(t => t.Created_At).OrderByDescending(t => t.Finished_At).First()
                    });
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public dynamic ProjectStruct(int proj_Id)
        {
            try
            {
                return projects.GroupJoin(
                    tasks,
                    p => p.Id,
                    t => t.Project_Id,
                    (proj, task) => new
                    {
                        Project = proj,
                        Tasks = task.ToList()
                    }).Join(
                    teams.GroupJoin(
                        users,
                        t => t.Id,
                        u => u.Team_Id,
                        (team, user) => new
                        {
                            Team = team,
                            Users = user.ToList()
                        }),
                    p => p.Project.Team_Id,
                    t => t.Team.Id,
                    (proj, team) => new
                    {
                        proj.Project,
                        proj.Tasks,
                        Team = team
                    }).Where(item => item.Project.Id == proj_Id).Select(item => new
                    {
                        item.Project,
                        LongestTask = item.Tasks.OrderByDescending(t => t.Description).FirstOrDefault(),
                        ShortestTask = item.Tasks.OrderBy(t => t.Name).FirstOrDefault(),
                        UsersCount = item.Project.Description.Length > 25 || item.Tasks.Count < 3 ? item.Team.Users.Count() : 0
                    });
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }
    }
}
