﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ.Models
{
    public enum State { Created, Started, Finished, Canceled}

    public class Task
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Created_At { get; set; }
        public DateTime Finished_At { get; set; }
        public State State { get; set; }
        public int Project_Id { get; set; }
        public int Performer_Id { get; set; }

        public override string ToString()
        {
            return $"Id:{this.Id} | Name: {Name}, Description: {Description}";
        }
    }
}
