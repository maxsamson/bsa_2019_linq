﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ.Models
{
    public class User
    {
        public int Id { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime Registered_At { get; set; }
        public int? Team_Id { get; set; }
    }
}
